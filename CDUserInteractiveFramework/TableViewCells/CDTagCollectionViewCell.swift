// Copyright (c) 2017 CasaDigi. All rights reserved.

import UIKit

public class CDTagCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak public var tagText: LingualLabel?
    @IBOutlet weak public var tagBackgroundView: UIView!
    public var isCheck: Bool = false

    @IBOutlet weak public var searchBtnLbl: InsetLabel!
    @IBOutlet weak public var crossBtn: UIButton!
    @IBOutlet weak public var searchField: SearchTextField!
    @IBOutlet weak public var tagImg: UIImageView?
    @IBOutlet weak public var backRoundImgView: UIImageView?
    
    public var aa1: String?
    
    override open var isSelected: Bool {
        didSet {
            if self.isSelected {
                self.tagImg?.image = UIImage(named: "Tag Sel Bg")
                //self.tagText?.textColor = UIColor.white
            } else {
                self.tagImg?.image = UIImage(named: "Btn-Back")
                //self.tagText?.textColor = UIColor(red: 133/255, green: 134/255, blue: 138/255, alpha: 1.0)
            }
        }
    }
}
