//
//  CDUserInteractiveFrameworkInteractionManager.swift
//  CDUserInteractiveFramework
//
//  Created by Navneet Singh Gill on 1/15/19.
//  Copyright © 2019 Paragon Business Solutions Ltd. All rights reserved.
//

import UIKit

public protocol CDUserInteractiveFrameworkDatasource {
    
    func getLanguageInfo() -> [String: Any]
    func getLanguageCode() -> String
    
}

public class CDUserInteractiveFrameworkInteractionManager: NSObject {

    public static let shared = CDUserInteractiveFrameworkInteractionManager()
    public var datasource: CDUserInteractiveFrameworkDatasource?
    
    func languageInfo() -> [String: Any]? {
        return datasource?.getLanguageInfo()
    }
    
    func languageCode() -> String? {
        return datasource?.getLanguageCode()
    }
    
}
