// Copyright (c) 2017 CasaDigi. All rights reserved.

import Foundation

@IBDesignable
public class FormTextField: UITextField {
    @IBInspectable public var inset: CGFloat = 0
    
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: inset + 10 , dy: inset - 5)
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
}

@objc public protocol CDSearchTextFieldDelegate {
    @objc optional func textFieldDidDeleteText(sourceView : UICollectionView,textField : UITextField)
    @objc optional func textFieldDidDeleteTextBack(textField : UITextField)
}

public class SearchTextField: UITextField {
    weak public var searchDelegate: CDSearchTextFieldDelegate?
    weak public var sourceCollectionView : UICollectionView?
    override public func deleteBackward() {
      
        if self.text?.count == 0 || self.alpha == 0.0 {
            //NotificationCenter.default.post(name: Notification.Name(rawValue: "emptyTextFieldBackspace"), object: nil)
            if sourceCollectionView != nil
            {
                searchDelegate?.textFieldDidDeleteText!(sourceView :sourceCollectionView!,textField : self)
            }
            else
            {
                searchDelegate?.textFieldDidDeleteTextBack!(textField : self)
            }
        }
        super.deleteBackward()
    }
    

}

public class InsetLabel: UILabel {
    override public func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)))
    }
}

@IBDesignable
open class LingualLabel: UILabel {
    private let languageInfo = CDUserInteractiveFrameworkInteractionManager.shared.languageInfo()
    
    @IBInspectable public var topinset: CGFloat = 0
    @IBInspectable public var leftinset: CGFloat = 0
    @IBInspectable public var baseText: String = "" { didSet { self.refreshText() } }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        UserDefaults.standard.addObserver(self, forKeyPath: "LanguageCode", options: .new, context: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        UserDefaults.standard.addObserver(self, forKeyPath: "LanguageCode", options: .new, context: nil)
    }
    
    override open func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topinset, left: leftinset, bottom: 0, right: 0)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    deinit {
        UserDefaults.standard.removeObserver(self, forKeyPath: "LanguageCode")
    }

    private func refreshText() {
        
        if let languageInfo = self.languageInfo, let languageInfoDictionary = languageInfo[baseText] as? [String:String], let languageCode = CDUserInteractiveFrameworkInteractionManager.shared.languageCode(), 
            let lingualText = languageInfoDictionary[languageCode] {
                self.text = lingualText
        } else {
            self.text = baseText
        }
    }
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "LanguageCode" {
            self.refreshText()
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}

@IBDesignable
public class DigiTextField: UITextField {
    @IBInspectable public var insetX: CGFloat = 0.0
    @IBInspectable public var insetY: CGFloat = 0.0
    
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX , dy: insetY)
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX , dy: insetY)
    }
}



@IBDesignable
open class LingualButton: UIButton {
    private let languageInfo = CDUserInteractiveFrameworkInteractionManager.shared.languageInfo()
    
    @IBInspectable open var baseTextNormal: String = "" {
        didSet {
            if let languageInfo = self.languageInfo, let languageInformation = languageInfo[baseTextNormal] as? [String:String], let languageCode = CDUserInteractiveFrameworkInteractionManager.shared.languageCode(),
                let lingualText = languageInformation[languageCode] {
                    self.setTitle(lingualText, for: UIControlState())
                    self.setTitle(lingualText, for: .highlighted)
                    self.setTitle(lingualText, for: .disabled)
                    
                    if baseTextSelected == nil {
                        self.setTitle(lingualText, for: .selected)
                    }
            } else {
                self.setTitle(baseTextNormal, for: UIControlState())
                self.setTitle(baseTextNormal, for: .highlighted)
                self.setTitle(baseTextNormal, for: .disabled)
            }
            
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable open var baseTextSelected: String? {
        didSet {
            if let languageInfo = self.languageInfo, let languageInformation = languageInfo[baseTextSelected!] as? [String:String], let languageCode = CDUserInteractiveFrameworkInteractionManager.shared.languageCode(),
                let lingualText = languageInformation[languageCode] {
                    self.setTitle(lingualText, for: .selected)
            } else {
                self.setTitle(baseTextSelected, for: .selected)
            }
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        UserDefaults.standard.addObserver(self, forKeyPath: "LanguageCode", options: .new, context: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        UserDefaults.standard.addObserver(self, forKeyPath: "LanguageCode", options: .new, context: nil)
    }
    
    deinit {
        UserDefaults.standard.removeObserver(self, forKeyPath: "LanguageCode")
    }
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "LanguageCode", let languageCode = change?[NSKeyValueChangeKey.newKey] as? String {
            if let languageInfo = self.languageInfo, let languageInformation = languageInfo[baseTextNormal] as? [String:String],
                let lingualText = languageInformation[languageCode] {
                    self.setTitle(lingualText, for: UIControlState())
                    self.setTitle(lingualText, for: .highlighted)
                    self.setTitle(lingualText, for: .disabled)
            } else {
                self.setTitle(baseTextNormal, for: UIControlState())
                self.setTitle(baseTextNormal, for: .highlighted)
                self.setTitle(baseTextNormal, for: .disabled)
            }
            
            if baseTextSelected != nil, let languageInfo = self.languageInfo, let languageInfoDic = languageInfo[baseTextSelected!] as? [String:String],
                let lingualText = languageInfoDic[languageCode] {
                    self.setTitle(lingualText, for: .selected)
            } else {
                self.setTitle(baseTextSelected, for: .selected)
            }
            
            self.setNeedsLayout()
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}
