//
//  CDSearchTagView.swift
//  CDUserInteractiveFramework
//
//  Created by Navneet Singh Gill on 1/15/19.
//  Copyright © 2019 Paragon Business Solutions Ltd. All rights reserved.
//

import UIKit

protocol CDSearchTagViewDelegate {
    func crossButtonTapped(button: UIButton)
}

public class CDSearchTagView: UIView {
    
    var searchTagViewDelegate: CDSearchTagViewDelegate?
    
    public override var bounds: CGRect {
        didSet {
            view?.frame = self.bounds
        }
    }
    
    public override var frame: CGRect {
        didSet {
            view?.frame = self.bounds
        }
    }
    
    @IBOutlet weak private var view: UIView? {
        didSet {
            view?.frame = self.bounds
        }
    }
    @IBOutlet weak private var crossButton: UIButton!
    
    @IBOutlet public var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = dataSourceCollectionView
            collectionView.delegate = delegateCollectionView
            
            initialiseNibs()
        }
    }
    
    @IBOutlet weak open var dataSourceCollectionView: UICollectionViewDataSource? {
        didSet {
            if let collectionView = collectionView {
                collectionView.dataSource = dataSourceCollectionView
            }
        }
    }
    @IBOutlet weak open var delegateCollectionView: UICollectionViewDelegate? {
        didSet {
            if let collectionView = collectionView {
                collectionView.delegate = delegateCollectionView
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialise()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialise()
    }
    
    func initialise() {
        
        if let view = Bundle(for: self.classForCoder).loadNibNamed("CDSearchTagView", owner: self, options: nil)?[0] as? UIView {
            self.view = view
//            view.frame = self.bounds
            self.addSubview(view)
            
            view.translatesAutoresizingMaskIntoConstraints = false
            let leadingConstraint = NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leadingMargin, multiplier: 1, constant: 0)
            let trailingConstraint = NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailingMargin, multiplier: 1, constant: 0)
            let topConstraint = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .topMargin, multiplier: 1, constant: 0)
            let bottomConstraint = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottomMargin, multiplier: 1, constant: 0)
            leadingConstraint.isActive = true
            trailingConstraint.isActive = true
            topConstraint.isActive = true
            bottomConstraint.isActive = true
            self.addConstraints([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
            
            self.layoutIfNeeded()
            
            self.backgroundColor = .green
            view.backgroundColor = .red
            
        }
    }
    
    func initialiseNibs() {
        if collectionView != nil {
            let searchNib = UINib(nibName: "CDSearchTagViewSearchCollectionViewCell", bundle: Bundle(for: self.classForCoder))
            self.collectionView.register(searchNib, forCellWithReuseIdentifier: "searchCell")
            
            let buttonNib = UINib(nibName: "CDSearchTagViewButtonCollectionViewCell", bundle: Bundle(for: self.classForCoder))
            self.collectionView.register(buttonNib, forCellWithReuseIdentifier: "btnCell")
        }
    }
    
    //MARK:- IBAction methods
    
    @IBAction func crossButtonTapped(_ button: UIButton) {
        searchTagViewDelegate?.crossButtonTapped(button: button)
    }

}
//
//class Cat: UIView {
//
//    override var bounds: CGRect {
//        didSet {
//
//        }
//    }
//
//    override var frame: CGRect {
//        didSet {
//
//        }
//    }
//
//}
