//
//  CDUserInteractiveFramework.h
//  CDUserInteractiveFramework
//
//  Created by Navneet Singh Gill on 1/15/19.
//  Copyright © 2019 Paragon Business Solutions Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CDUserInteractiveFramework.
FOUNDATION_EXPORT double CDUserInteractiveFrameworkVersionNumber;

//! Project version string for CDUserInteractiveFramework.
FOUNDATION_EXPORT const unsigned char CDUserInteractiveFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CDUserInteractiveFramework/PublicHeader.h>


